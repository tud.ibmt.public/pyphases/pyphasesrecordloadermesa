from typing import List
from pyPhasesRecordloaderSHHS.recordLoaders.SHHSAnnotationLoader import SHHSAnnotationLoader
from pyPhasesRecordloader import Event


class MESAAnnotationLoader(SHHSAnnotationLoader):
    
    eventMapLeg = {
        "Limb movement - left|Limb Movement (Left)": "LegMovement-Left",
        "Periodic leg movement - left|PLM (Left)": "LegMovement-Left",
    }

    def loadAnnotation(self, xmlFile) -> List[Event]:
        allEvents = super().loadAnnotation(xmlFile)

        allEvents += self.loadEvents(
            "[EventType='Stages|Stages']",
            self.eventMapLeg,
        )

        return allEvents