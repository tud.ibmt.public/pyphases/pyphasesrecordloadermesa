from unittest import TestCase

from pyPhases import ConfigNotFoundException, Project, pdict

from pyPhasesRecordloaderMESA.Plugin import Plugin
from pyPhasesRecordloader import RecordLoader


class TestPlugin(TestCase):
    def test_loaderMisconfigured(self):
        project = Project()
        project.config = pdict({})
        plugin = Plugin(project)

        # useLoader not specified
        self.assertRaises(ConfigNotFoundException, plugin.initPlugin)
        self.project.config = pdict({"useLoader": "mesa"})
        plugin = Plugin(self.project)
        self.assertRaises(ConfigNotFoundException, plugin.initPlugin)

    def setUp(self):
        self.options = {}
        self.project = Project()
        self.project.addPlugin("pyPhasesRecordloaderMESA", self.options)
        self.plugin = self.project.plugins[-1]
        self.project.config.update({"useLoader": "mesa", "mesa-path": "./data"})

    def test_initPlugin(self):

        self.plugin.initPlugin()

        self.assertIn("RecordLoaderMESA", RecordLoader.recordLoaders)
        self.assertIn("MESAAnnotationLoader", RecordLoader.recordLoaders)
        self.assertIn("mesa", self.project.config["loader"])

        self.assertEqual(self.project.config["loader"]["mesa"]["dataBase"], "mesa")
        self.assertEqual(
            self.project.config["loader"]["mesa"]["dataset"]["downloader"]["basePath"],
            "data",
        )
        self.assertEqual(
            self.project.getConfig("loader.mesa.dataset.downloader.basePathExtensionwise")[0],
            "data/polysomnography/edfs",
        )
